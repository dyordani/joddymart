<div class="row">
  <h3><?php echo lang('edit_user_heading');?></h3>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-light">

       <?php echo lang('edit_user_subheading');?>
      
      </div>
      <div class="card-body load-data">
        <div id="infoMessage"><?php echo $message;?></div>
          <div class="col-md-6">
            <?php echo form_open(uri_string());?>
            <?php echo form_hidden('id', $user->id);?>
            <?php echo form_hidden($csrf); ?>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('edit_user_fname_label', 'first_name');?></label>
                <?php echo form_input($first_name);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('edit_user_lname_label', 'last_name');?></label>
                <?php echo form_input($last_name);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('edit_user_company_label', 'company');?></label>
                <?php echo form_input($company);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('edit_user_phone_label', 'phone');;?></label>
                <?php echo form_input($phone);?>
            </div>
             <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('edit_user_password_label', 'password');?></label>
                <?php echo form_input($password);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
                <?php echo form_input($password_confirm);?>
            </div>
            <?php if ($this->ion_auth->is_admin()): ?>

          <h3><?php echo lang('edit_user_groups_heading');?></h3>
          <?php foreach ($groups as $group):?>
              <label class="checkbox">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
          <?php endforeach?>

          <?php endif ?>
            <br/>
             <input type="submit" name="f_save" class="btn btn-primary" value="Save">
           <?php echo form_close();?>
          </div>
      </div>
    </div>
  </div>
</div>