<div class="row">
	<h3>List <?php echo lang('index_heading');?></h3>
	<div class="col-md-12">
		<div class="card">
			<div class="card-header bg-light">

				<a href="<?=base_url('auth/create_user')?>">
					<button class="btn btn-outline-primary btn-add-user" >Add Users</button>
				</a>
				
				<a href="<?=base_url('auth/create_group')?>">
					<button class="btn btn-outline-primary btn-add-group" >Create Group</button>
				</a>
				
			</div>
			<div class="card-body load-data">
				<?php if(isset($users)):?>
					<div class="table-responsive">
						<table class="table table-bordered" id="myTable">
							<thead>
								<th width="5%">No</th>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_groups_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th width="20%">&nbsp;</th>
							</thead>
							<tbody>
								<?php $no = 1;?>
								<?php foreach ($users as $user):?>
								<tr>
									<td><?=$no?></td>
									<td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
						            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
						            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
									<td>
										<?php foreach ($user->groups as $group):?>
											<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
						                <?php endforeach?>
									</td>
									<td>
										<?php if($user->active):?>
											<a href="<?=base_url("auth/deactivate/".$user->id)?>">
												<button type="button" class="btn btn-info" >
													<i class="fa fa-pen-square"></i> &nbsp; <?=lang('index_active_link')?>
												</button>
											</a>
										<?php else:?>
											<a href="<?=base_url("auth/activate/".$user->id)?>">
												<button type="button" class="btn btn-danger" >
													<i class="fa fa-pen-square"></i> &nbsp; <?=lang('index_inactive_link')?>
												</button>
											</a>
										<?php endif;?>


									</td>
									<td>
										
										<a href="<?=base_url("auth/edit_user/".$user->id)?>">
											<button type="button" class="btn btn-info" >
												<i class="fa fa-pen-square"></i> &nbsp; Edit
											</button>
										</a>
									</td>
								</tr>
								<?php $no++;?>
								<?php endforeach;?>
							</tbody>
						</table>
						
					</div>
				<?php else:?>
					Empty ...
				<?php endif;?>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-small" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>