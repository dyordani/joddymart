<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Cpanel - Administrator </title>
    <link rel="stylesheet" href="<?=base_url('assets')?>/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?=base_url('assets')?>/vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?=base_url('assets')?>/css/styles.css">
</head>
<body>
<div class="page-wrapper flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">

                <div class="card p-4">
                    <div class="card-header text-center text-uppercase h4 font-weight-light">
                        Login
                        
                    </div>


                    <?php echo form_open("auth/login");?>
                    <div class="card-body py-5">
                        <div id="infoMessage" class="text-center" style="background-color: red;color: white"><?php echo $message;?></div>
                        <div class="form-group">
                            <label class="form-control-label">Email</label>
                           <?php echo form_input($identity);?>
                        </div>

                        <div class="form-group">
                            <label class="form-control-label">Password</label>
                             <?php echo form_input($password);?>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <?php echo form_submit('submit', lang('login_submit_btn'),"class='btn btn-primary px-5'");?>
                                
                            </div>

                            <div class="col-6">
                                <a href="<?=base_url('auth/forgot_password')?>" class="btn btn-link"><?php echo lang('login_forgot_password');?></a>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url('assets')?>/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url('assets')?>/vendor/popper.js/popper.min.js"></script>
<script src="<?=base_url('assets')?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets')?>/vendor/chart.js/chart.min.js"></script>
<script src="<?=base_url('assets')?>/js/carbon.js"></script>
<script src="<?=base_url('assets')?>/js/demo.js"></script>
</body>
</html>
