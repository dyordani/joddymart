<div class="row">
  <h3><?php echo lang('create_user_heading');?></h3>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-light">

       <?php echo lang('create_user_subheading');?>
      
      </div>
      <div class="card-body load-data">
        <div id="infoMessage"><?php echo $message;?></div>
          <div class="col-md-6">
            <?php echo form_open("auth/create_user");?>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_fname_label', 'first_name');?></label>
                <?php echo form_input($first_name);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_lname_label', 'last_name');?></label>
                <?php echo form_input($last_name);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_identity_label', 'identity');?></label>
                <?php echo form_input($identity);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_company_label', 'company');?></label>
                <?php echo form_input($company);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_email_label', 'email');?></label>
                <?php echo form_input($email);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_phone_label', 'phone');?></label>
                <?php echo form_input($phone);?>
            </div>
             <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_password_label', 'password');?></label>
                <?php echo form_input($password);?>
            </div>
            <div class="form-group">
                <label for="normal-input" class="form-control-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
                <?php echo form_input($password_confirm);?>
            </div>

             <input type="submit" name="f_save" class="btn btn-primary" value="Save">
           <?php echo form_close();?>
          </div>
      </div>
    </div>
  </div>
</div>