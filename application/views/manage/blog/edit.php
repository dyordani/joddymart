<script type="text/javascript">
    CKEDITOR.replace("f_description");
</script>

<div class="modal-header bg-primary">
    <h5 class="modal-title text-white">Update Blog</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<?=form_open_multipart('manage/blog/update')?>
<input type="hidden" name="f_uc" value="<?=$row->uc?>">
<input type="hidden" name="f_old_image" value="<?=$row->image?>">
<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Title</label>
            <input id="normal-input" class="form-control" name="f_title" value="<?=$row->title?>">
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Description</label>
            <textarea class="" name="f_description"><?php echo htmlspecialchars_decode(stripslashes($row->description))?></textarea>
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Author</label>
            <input id="normal-input" class="form-control" name="f_author" value="<?=$row->author?>">
        </div>

        <div class="form-group">
            <label for="normal-input" class="form-control-label">Image</label>
            <div class="img-responsive">
                 <img src="<?=base_url('uploads/blog/'.$row->image)?>" class="img img-thumbnail">
            </div>
           
        </div>

        <div class="form-group">
            <label for="normal-input" class="form-control-label">Replace With :</label>
            <input id="normal-input" class="form-control" name="f_image" type="file" ">
        </div>

    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    <input type="submit" name="f_save" class="btn btn-primary" value="Save">
</div>
<?=form_close()?>