<script type="text/javascript" src="<?=base_url('assets/js/jquery-mask.js')?>"></script>
<script type="text/javascript">
    CKEDITOR.replace("f_product_description");

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.uang').mask('000.000.000', {reverse: true});

        $('input[name=f_discount]').keyup(function(){
            var price = $('input[name=f_price_before_discount]').val();
            var tax = $(this).val();

            var price_int  = parseInt(price);
            var tax_int  = parseInt(tax);

            var total = tax_int/100*price_int;

            var gelo = parseInt(total).toLocaleString();

            $('input[name=f_price_after_discount]').val(total);
        });
    });
</script>

<div class="modal-header bg-primary">
    <h5 class="modal-title text-white">Add Product</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<?=form_open_multipart('manage/product/insert')?>
<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Product Name</label>
            <input id="normal-input" class="form-control" name="f_product_name" required="">
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Description</label>
            <textarea class="" name="f_product_description"></textarea>
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Category</label>
            <select name="f_category" class="form-control">
                <option value="">--- Choose ---</option>
                <?php if(isset($category)):?>
                    <?php foreach($category as $cat):?>
                        <option value="<?=$cat->uc?>"><?=$cat->category?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Image</label>
            <input id="normal-input" class="form-control" name="f_image" type="file" ">
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Stock</label>
            <input id="normal-input" class="form-control" name="f_stock" required="" type="number">
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Package</label>
            <select name="f_package" class="form-control">
                <option value="">--- Choose ---</option>
                <?php if(isset($package)):?>
                    <?php foreach($package as $cat):?>
                        <option value="<?=$cat->uc?>"><?=$cat->package?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
        </div>

        <div class="form-group">
            <label for="normal-input" class="form-control-label">Price (Rp.)</label>
            <input id="normal-input" class="form-control " name="f_price_before_discount" required="" style="text-align: right;">
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Discount (%)</label>
            <input id="normal-input" class="form-control " name="f_discount"   style="text-align: right;">
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Price After Discount (Rp.)</label>
            <input id="normal-input" class="form-control " name="f_price_after_discount" " style="text-align: right;" readonly="">
        </div>

    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    <input type="submit" name="f_save" class="btn btn-primary" value="Save">
</div>
<?=form_close()?>