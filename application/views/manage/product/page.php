<script type="text/javascript">
	$(document).ready(function(){
		var base_url = $('#base-url').html();
		

		$('.page a.pagination-ajax').click(function(){
			var page = $(this).attr('title');
			var product_name = $('input[name=f_product_fill]').val();
			var category = $('select[name=f_category_fill] option:selected').val();

			$('.load-data').load(
									base_url+'manage/product/page',{
										js_page : page,
										js_product_name : product_name,
										js_category : category,
										'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
									}
								);

			return false;
			
		});

		$('.btn-search').click(function(){
			var product_name = $('input[name=f_product_fill]').val();
			var category = $('select[name=f_category_fill] option:selected').val();
			var page = 1;

			$('.load-data').load(
									base_url+'manage/product/page',{
										js_page : page,
										js_product_name : product_name,
										js_category : category,
										'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
									}
								);

			return false;
		});
	});
</script>
<?php if(isset($result)):?>
	<div class="table-responsive">
		<table class="table table-bordered" id="myTable">
			<thead>
				<tr>
					<th width="5%" rowspan="2" class="text-center">No</th>
					<th width="15%" rowspan="2" class="text-center">Image</th>
					<th rowspan="2" class="text-center">Product Name</th>
					<th rowspan="2" class="text-center">Category</th>
					<th colspan="2" class="text-center">Stock</th>
					<th rowspan="2" width="10%" class="text-center">Create Time</th>
					<th rowspan="2" width="16%" class="text-center">Update</th>
					<th rowspan="2" width="16%" class="text-center">Action</th>
				</tr>
				<tr>
					
					<th class="text-center">In</th>
					<th class="text-center">Out</th>
				</tr>
				
			</thead>
			<tbody>
				<?php $no = 1;?>
				<?php foreach($result as $row):?>
				<tr>
					<td class="text-center"><?=$no?></td>
					<td class="text-center"><img src="<?=base_url('uploads/product/'.$row->product_image)?>" class="img img-responsive" width="50%"></td>
					<td class="text-center"><?=$row->product_name?></td>
					<td class="text-center"><?=$row->category?></td>
					<td class="text-right"><?=$row->stock_awal?></td>
					<td class="text-right"><?=$row->stock_keluar?></td>
					<td class="text-center"><?=time_format($row->create_time, 'd M Y H:i')?></td>
					<td>
						<a href="<?=base_url('manage/product/price/'.$row->uc)?>">
							<button type="button" class="btn btn-warning" >
							<i class="fa fa-pen-square"></i> &nbsp; Price
							</button>
						</a>
						
						<button type="button" class="btn btn-warning btn-stock" data-toggle="modal" data-target="#modal-1" uc="<?=$row->uc?>">
						<i class="fa fa-pen-square"></i> &nbsp; Stock
						</button>
					</td>
					<td>
						<button type="button" class="btn btn-info btn-edit" data-toggle="modal" data-target="#modal-1" uc="<?=$row->uc?>">
						<i class="fa fa-pen-square"></i> &nbsp; Edit
						</button>

						<a href="<?=base_url('manage/product/delete/'.$row->uc)?>" onclick="return confirm('Are you sure want to delete?')">
						<button type="button" class="btn btn btn-danger">
						<i class="fa fa-trash"></i> &nbsp; Delete
						</button>
						</a>

					</td>
				</tr>
				<?php $no++;?>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<div class="im-pagination page">
		<?php if (isset($pagination)) : ?>
			<?=$pagination?>
		<?php endif; ?>
	</div>
<?php else:?>
	Empty ...
<?php endif;?>