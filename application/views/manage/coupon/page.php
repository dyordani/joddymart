<script type="text/javascript">
	$(document).ready(function(){
		var base_url = $('#base-url').html();

		$('.page a.pagination-ajax').click(function(){
			var page = $(this).attr('title');


			$('.load-data').load(
									base_url+'manage/coupon/page',{
										js_page : page,
										'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
									}
								);

			return false;
			
		});
	});
</script>

<?php if(isset($result)):?>
	<div class="table-responsive">
		<table class="table table-bordered" id="myTable">
			<thead>
				<th width="5%">No</th>
				<th>Code Coupon</th>
				<th>Value</th>
				<th>Time</th>
				<th width="20%">&nbsp;</th>
			</thead>
			<tbody>
				<?php $no = 1;?>
				<?php foreach($result as $row):?>
				<tr>
					<td><?=$no?></td>
					<td><?=$row->code_coupon?></td>
					<td>Rp. <?=value_format($row->value)?></td>
					<td><?=time_format($row->start_date, 'd M Y')?> - <?=time_format($row->end_date, 'd M Y')?></td>
					<td>
						<button type="button" class="btn btn-info btn-edit" data-toggle="modal" data-target="#modal-1" uc="<?=$row->uc?>">
						<i class="fa fa-pen-square"></i> &nbsp; Edit
						</button>

						<a href="<?=base_url('manage/product/delete_cat/'.$row->uc)?>" onclick="return confirm('Are you sure want to delete?')">
						<button type="button" class="btn btn btn-danger">
						<i class="fa fa-trash"></i> &nbsp; Delete
						</button>
						</a>

					</td>
				</tr>
				<?php $no++;?>
				<?php endforeach;?>
			</tbody>
		</table>
		
	</div>
	<div class="im-pagination page">
		<?php if (isset($pagination)) : ?>
			<?=$pagination?>
		<?php endif; ?>
	</div>
<?php else:?>
	Empty ...
<?php endif;?>