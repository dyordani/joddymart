<div class="modal-header bg-primary">
    <h5 class="modal-title text-white">Add Coupon</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<?=form_open('manage/coupon/insert')?>
<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Code Coupon</label>
            <input id="normal-input" class="form-control" name="f_code_coupon" >
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Value (Rp.)</label>
            <input id="normal-input" class="form-control" name="f_value" >
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Start Date</label>
            <input id="normal-input" class="form-control" type="date" name="f_start_date" >
        </div>
        <div class="form-group">
            <label for="normal-input" class="form-control-label">End Date</label>
            <input id="normal-input" class="form-control" type="date" name="f_end_date" >
        </div>

    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    <input type="submit" name="f_save" class="btn btn-primary" value="Save">
</div>
<?=form_close()?>