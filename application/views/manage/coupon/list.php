<script type="text/javascript">
	$(document).ready(function(){
		var base_url = $('#base-url').html();
		$('.btn-add').click(function(){
			$('.modal-content').load(base_url+'manage/coupon/add');

		});

		$('.btn-edit').click(function(){
			var uc = $(this).attr('uc');
			var token

			$('.modal-content').load(base_url+'manage/coupon/edit',{
				js_uc : uc, 
				'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
			});

		});

		$('.page a.pagination-ajax').click(function(){
			var page = $(this).attr('title');


			$('.load-data').load(
									base_url+'manage/coupon/page',{
										js_page : page,
										'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
									}
								);

			return false;
			
		});
	});
</script>
<div class="row">
	<h3>List Coupon</h3>
	<div class="col-md-12">
		<div class="card">
			<div class="card-header bg-light">

				<button class="btn btn-outline-primary btn-add" data-toggle="modal" data-target="#modal-1">Add Coupon</button>
			
			</div>
			<div class="card-body load-data">
				<?php if(isset($result)):?>
					<div class="table-responsive">
						<table class="table table-bordered" id="myTable">
							<thead>
								<th width="5%">No</th>
								<th>Code Coupon</th>
								<th>Value</th>
								<th>Time</th>
								<th width="20%">&nbsp;</th>
							</thead>
							<tbody>
								<?php $no = 1;?>
								<?php foreach($result as $row):?>
								<tr>
									<td><?=$no?></td>
									<td><?=$row->code_coupon?></td>
									<td>Rp. <?=value_format($row->value)?></td>
									<td><?=time_format($row->start_date, 'd M Y')?> - <?=time_format($row->end_date, 'd M Y')?></td>
									<td>
										<button type="button" class="btn btn-info btn-edit" data-toggle="modal" data-target="#modal-1" uc="<?=$row->uc?>">
										<i class="fa fa-pen-square"></i> &nbsp; Edit
										</button>

										<a href="<?=base_url('manage/product/delete_cat/'.$row->uc)?>" onclick="return confirm('Are you sure want to delete?')">
										<button type="button" class="btn btn btn-danger">
										<i class="fa fa-trash"></i> &nbsp; Delete
										</button>
										</a>

									</td>
								</tr>
								<?php $no++;?>
								<?php endforeach;?>
							</tbody>
						</table>
						
					</div>
					<div class="im-pagination page">
						<?php if (isset($pagination)) : ?>
							<?=$pagination?>
						<?php endif; ?>
					</div>
				<?php else:?>
					Empty ...
				<?php endif;?>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-small" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>