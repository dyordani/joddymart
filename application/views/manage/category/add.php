<div class="modal-header bg-primary">
    <h5 class="modal-title text-white">Add Category Product</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<?=form_open('manage/product/insert_cat')?>
<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label for="normal-input" class="form-control-label">Category Product</label>
            <input id="normal-input" class="form-control" name="f_category" >
        </div>

    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    <input type="submit" name="f_save" class="btn btn-primary" value="Save">
</div>
<?=form_close()?>