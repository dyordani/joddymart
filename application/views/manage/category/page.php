<script type="text/javascript">
	$(document).ready(function(){
		var base_url = $('#base-url').html();


		$('.page a.pagination-ajax').click(function(){
			var page = $(this).attr('title');


			$('.load-data').load(
									base_url+'manage/product/page_category',{
										js_page : page,
										'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
									}
								);

			return false;
			
		});
	});
</script>

<?php if(isset($result)):?>
	<div class="table-responsive">
		<table class="table table-bordered" id="myTable">
			<thead>
				<th width="5%">No</th>
				<th>Category</th>
				<th width="20%">&nbsp;</th>
			</thead>
			<tbody>
				<?php $no = 1;?>
				<?php foreach($result as $row):?>
				<tr>
					<td><?=$no?></td>
					<td><?=$row->category?></td>
				
					<td>
						<button type="button" class="btn btn-info btn-edit" data-toggle="modal" data-target="#modal-1" uc="<?=$row->uc?>">
						<i class="fa fa-pen-square"></i> &nbsp; Edit
						</button>

						<a href="<?=base_url('manage/product/delete_cat/'.$row->uc)?>" onclick="return confirm('Are you sure want to delete?')">
						<button type="button" class="btn btn btn-danger">
						<i class="fa fa-trash"></i> &nbsp; Delete
						</button>
						</a>

					</td>
				</tr>
				<?php $no++;?>
				<?php endforeach;?>
			</tbody>
		</table>
		
	</div>
	<div class="im-pagination page">
		<?php if (isset($pagination)) : ?>
			<?=$pagination?>
		<?php endif; ?>
	</div>
<?php else:?>
	Empty ...
<?php endif;?>