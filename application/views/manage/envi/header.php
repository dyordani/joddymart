    <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cpanel Joddy Mart - Dashboard</title>
    <link rel="stylesheet" href="<?=base_url('assets/vendor/simple-line-icons/css/simple-line-icons.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/vendor/font-awesome/css/fontawesome-all.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/styles.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/pop-up.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/third_party/jquery-ui/jquery-ui.css')?>">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/> -->
    <script type="text/javascript" src="<?=base_url('assets/js/jquery-1.11.1.min.js');?>"></script>
    
    <script type="text/javascript" src="<?=base_url('assets/third_party/jquery-ui/jquery-ui.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/third_party/jquery-ui/jquery-timepicker.js');?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/ckeditor/ckeditor.js');?>"></script>
    
    <script type="text/javascript">
        $(document).ready(function(){

            var base_url = $('#base-url').html();

            // $('.btn-profile').click(function(){
            //     $('.modal-content').load(base_url+'profile/edit');
            // });

            // $('.btn-laporan-bulan').click(function(){
            //     $('.modal-content').load(base_url+'laporan/get_report_month');
            // });

            //  $('.btn-laporan-tahun').click(function(){
            //     $('.modal-content').load(base_url+'laporan/get_report_year');
            // });


        });
    </script>
    <style type="text/css">
        /* BEGIN of PAGINATION */
.im-pagination{
    width: 495px;
    height: 21px;
    overflow: hidden;
    font-family: arial;
    font-size: 9pt;
    margin-top: 20px;
    margin-bottom: 10px;
    line-height: 11pt;
}

.im-pagination span{
    width: auto;
    min-width: 15px;
    display: block;
    margin: 1px 2px;
    float: left;
    text-align: center;
    border-radius: 2px;
}

.im-pagination span a{
    display: block;
    padding: 2px 7px;
    color: #3E3E3E;
    font-size: 9pt;
    border-radius: 2px;
}

.im-pagination span a:hover{
    background-color: #D7D7D7;
}

.im-pagination span.page-nav{
    margin-top: 0;
}

.im-pagination span.current-page{
    padding: 2px 3px;
    background-color: #242424;
    color: #ffffff;
}

.im-pagination span.nav-off{
    padding: 1px 7px;
    color: #ABABAB;
}
/* END of PAGINATION */
    </style>
</head>
<body class="sidebar-fixed header-fixed">
<div id="base-url" style="display: none"><?=base_url()?></div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: "-40:+0"
        });
    });
</script>
<div id="base-url" style="display: none"><?=base_url()?></div>
<div class="page-wrapper">
    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto">
            <i class="fa fa-bars"></i>
        </a>

        <a class="navbar-brand" href="<?=base_url('dashboard')?>">
            <h2>Panel Administrator</h2>
        </a>

        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="fa fa-bars"></i>
        </a>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="<?=base_url('assets/imgs/avatar-1.png')?>" class="avatar avatar-sm" alt="logo">
                    <span class="small ml-1 d-md-down-none"><?=$this->session->userdata('email')?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header">Account</div>

                    <a data-toggle="modal" data-target="#modal-1" class="dropdown-item btn-profile" ">
                        <i class="fa fa-user"></i> Change Profile
                    </a>
                     <a href="#" class="dropdown-item">
                        <i class="fa fa-lock-open"></i> Change Password
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="fa fa-envelope"></i> Messages
                    </a>
                    <a href="<?=base_url('auth/logout')?>" class="dropdown-item">
                        <i class="fa fa-lock"></i> Logout
                    </a>
                </div>
            </li>
        </ul>
    </nav>

    <div class="main-container">
       <div class="sidebar ">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-title">Product & Transaction</li>

                     <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-tag"></i> Product <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?=base_url('manage/product/category')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Category Product
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('manage/product')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Product
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('manage/product/realtime')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Real Time Product
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-credit-card"></i> Transaction <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?=base_url('manage/orders')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Orders
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('manage/orders/payment_confirm')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Payment Confirm
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url('manage/customer')?>" class="nav-link">
                            <i class="icon icon-people"></i> Customer
                        </a>
                    </li>


                    <li class="nav-title">Orther</li>
                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-support"></i> Orther <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?=base_url('manage/blog')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Blog
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('manage/coupon')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Coupon
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('manage/package')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Package Product
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('auth')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Users
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-title">Report</li>
                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-doc"></i> Report <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?=base_url('upt')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Transaction
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('mata_pelajaran')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Product
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('mata_pelajaran')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Contact
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('mata_pelajaran')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Review Product
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('mata_pelajaran')?>" class="nav-link">
                                    <i class="icon  icon-target"></i> Massage
                                </a>
                            </li>
                        </ul>
                    </li>

                   

                  
                </ul>

            </nav>
        </div>

        <div class="content">
            <div class="container-fluid">
