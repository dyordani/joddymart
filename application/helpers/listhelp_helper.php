<?php

function list_user($filter = NULL){
	$CI =& get_instance();
	$CI->load->model('user_m');
	
	if ($filter != NULL) {		
		$query = $CI->user_m->get_filtered($filter);
	}
	else {
		$query = $CI->user_m->get_all();	
	}
	
	if ($query->num_rows() > 0) {
		return $query->result();
	}
	else {
		return NULL;
	}
}

function list_kecamatan($filter = NULL){
	$CI =& get_instance();
	$CI->load->model('kecamatan_m');
	
	if ($filter != NULL) {		
		$query = $CI->kecamatan_m->get_filtered($filter);
	}
	else {
		$query = $CI->kecamatan_m->get_all();	
	}
	
	if ($query->num_rows() > 0) {
		return $query->result();
	}
	else {
		return NULL;
	}
}

function list_upt($filter = NULL){
	$CI =& get_instance();
	$CI->load->model('Upt_m');
	
	if ($filter != NULL) {		
		$query = $CI->Upt_m->get_filtered($filter);
	}
	else {
		$query = $CI->Upt_m->get_all();	
	}
	
	if ($query->num_rows() > 0) {
		return $query->result();
	}
	else {
		return NULL;
	}
}

function list_pejabat_ttd($filter = NULL){
	$CI =& get_instance();
	$CI->load->model('Pejabat_ttd_m');
	
	if ($filter != NULL) {		
		$query = $CI->Pejabat_ttd_m->get_filtered($filter);
	}
	else {
		$query = $CI->Pejabat_ttd_m->get_all();	
	}
	
	if ($query->num_rows() > 0) {
		return $query->result();
	}
	else {
		return NULL;
	}
}

function list_buku_induk($filter = NULL){
	$CI =& get_instance();
	$CI->load->model('Buku_induk_m');
	
	if ($filter != NULL) {		
		$query = $CI->Buku_induk_m->get_filtered($filter);
	}
	else {
		$query = $CI->Buku_induk_m->get_all();	
	}
	
	if ($query->num_rows() > 0) {
		return $query->result();
	}
	else {
		return NULL;
	}
}

function list_pegawai($filter = NULL){
	$CI =& get_instance();
	$CI->load->model('Pegawai_m');
	
	if ($filter != NULL) {		
		$query = $CI->Pegawai_m->get_filtered($filter);
	}
	else {
		$query = $CI->Pegawai_m->get_all();	
	}
	
	if ($query->num_rows() > 0) {
		return $query->result();
	}
	else {
		return NULL;
	}
}

function read_title($string){
	$string = htmlspecialchars_decode(stripslashes(mb_convert_encoding($string,"HTML-ENTITIES","UTF-8")));

	return $string;
}

function read_text($string){
	$string = htmlspecialchars_decode(stripslashes($string));

	return $string;
}

?>