<?php
Class Product_m extends MY_Model{
	function __construct(){
		parent::__construct();

		$this->table_name = 'ol_product';
	}

	function get_list($filter = NULL,$limit = NULL, $offset = 0){
		$sql = " SELECT p.*, pc.category
				FROM `ol_product` p
				LEFT JOIN `ol_product_category` pc ON p.uc_category = pc.uc ";
		$sql .= "WHERE p.is_exist = '1' ";		

		if (@$filter['product_name'] != NULL) {
			$sql .= " AND p.`product_name` = LIKE '%".$filter['product_name']."%' ";
		}

		if (@$filter['uc_category'] != NULL) {
			$sql .= " AND p.`uc_category` = '".$filter['uc_category']."' ";
		}

		if ($limit != NULL) {
			$sql .= "  LIMIT ".$offset.", ".$limit." ";
		}

		echo $sql;

		return $this->exec_query($sql);
	}
}