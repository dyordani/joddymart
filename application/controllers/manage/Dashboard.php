<?php
Class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in())
	    {
	      	if (!$this->ion_auth->is_admin())
		    {
		      	redirect('auth');
		    }
	    }
	}

	function index(){
		
		//print_r($this->session->userdata());
		$this->im_render->main_admin('manage/dashboard');
	}
}