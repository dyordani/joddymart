<?php
Class Blog extends CI_Controller{
	function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in())
	    {
	      	if (!$this->ion_auth->is_admin())
		    {
		      	redirect('auth');
		    }
	    }

	    $this->each_page 	= 20;
		$this->page_int 	= 5;

		$this->load->model('blog_m');
	}

	function index(){
		$data = NULL;

		$page = 1;
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'blog_m'
						);

		$query = $this->blog_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->blog_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->im_render->main_admin('manage/blog/list', $data);

	}

	function page(){
		$data = NULL;

		$page = ($this->input->post('js_page') != 1 ? $this->input->post('js_page') : 1);
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'blog_m'
						);

		$query = $this->blog_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->blog_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->load->view('manage/blog/page', $data);
	}

	function add(){
		$this->load->view('manage/blog/add');
	}

	function insert(){
		if ($this->input->post('f_save')) {

			$this->load->library('im_upload');
      		$photo  = $this->im_upload->uploading('f_image', 'blog');

			$data = array(
				'uc' => unique_code(),
				'slug' => post_name($this->input->post('f_title')),
				'title' => $this->input->post('f_title'),
				'description' => htmlspecialchars(addslashes($this->input->post('f_description'))),
				'author' => $this->input->post('f_author'),
				'image' => $photo,
				'create_time' => current_time()
			);

			$this->blog_m->insert_data($data);
		}

		redirect('manage/blog');
	}

	function edit(){
		$data = NULL;

		$js_uc = $this->input->post('js_uc');

		$data['row'] = $this->blog_m->get_filtered(array('uc' => $js_uc))->row();

		$this->load->view('manage/blog/edit', $data);
	}

	function update(){
		if ($this->input->post('f_save')) {

			$this->load->library('im_upload');
      
			$image  = $this->input->post('f_old_image');

			if(preg_match('/image/', @$_FILES['f_image']['type'])){
				if (isset($_POST['f_old_image'])) {
					if (@$_FILES['f_image']['type'] != NULL) {
						$image = $this->im_upload->replacing($this->input->post('f_old_image'), 'f_image', 'blog');
					}
				}else{
					$image = $this->im_upload->uploading('f_image', 'blog');
				}
			}

			$data = array(
				'slug' => post_name($this->input->post('f_title')),
				'title' => $this->input->post('f_title'),
				'description' => htmlspecialchars(addslashes($this->input->post('f_description'))),
				'author' => $this->input->post('f_author'),
				'image' => $image
			);

			$where = array('uc' => $this->input->post('f_uc'));

			$this->blog_m->update_data($data, $where);
		}

		redirect('manage/blog');

	}

	function delete($uc = NULL){
		if ($uc != NULL) {
			$this->blog_m->delete_data(array('uc' => $uc));
		}

		redirect('manage/blog');
	}

	function view($uc = NULL){
		if ($uc != NULL) {
			$data = NULL;

			$data['row'] = $this->blog_m->get_filtered(array('uc' => $uc))->row();


		}else{
			redirect('manage/blog');
		}
	}
}