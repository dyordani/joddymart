<?php
Class Product extends CI_Controller{
	function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in())
	    {
	      	if (!$this->ion_auth->is_admin())
		    {
		      	redirect('auth');
		    }
	    }

	    $this->each_page 	= 20;
		$this->page_int 	= 5;

	    $this->load->model('product_m');
	    $this->load->model('product_cat_m');
	    $this->load->model('product_price_m');

	    $this->load->model('package_m');

	}

	/*PRODUCT*/
	function index(){
		$data = NULL;

		$page = 1;
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'product_m'
						);

		$query = $this->product_m->get_list(NULL,$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
 		}

 		$query = $this->product_m->get_list(NULL,$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
 		}

 		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;
 		$data['category'] = $this->product_cat_m->get_all()->result();

 		$this->im_render->main_admin('manage/product/list',$data);


	}

	function page(){
		$data = NULL;

		$page = ($this->input->post('js_page') != 1 ? $this->input->post('js_page') : 1);

		$filter['uc_category'] = ($this->input->post('js_category') != NULL ? $this->input->post('js_category') : NULL);
		$filter['product_name'] = ($this->input->post('js_product_name') != NULL ? $this->input->post('js_product_name') : NULL);

		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'product_m'
						);

		$query = $this->product_m->get_list($filter,$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
 		}

 		$query = $this->product_m->get_list($filter,$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
 		}

 		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

 		$this->load->view('manage/product/page', $data);
	}
         

	function add(){
		$data = NULL;

		$data['category'] = $this->product_cat_m->get_all()->result();
		$data['package'] = $this->package_m->get_all()->result();

		$this->load->view('manage/product/add', $data);
	}

	function insert(){
		if ($this->input->post('f_save')) {

			$this->load->library('im_upload'); 
      		$image  = $this->im_upload->uploading('f_image', 'product');

      		$uc_product = unique_code();
			$data = array(
				'uc' => $uc_product,
				'uc_category' => $this->input->post('f_category'),
				'uc_package' => $this->input->post('f_package'),
				'slug' => post_name($this->input->post('f_product_name')),
				'product_name' => $this->input->post('f_product_name'),
				'product_description' => $this->input->post('f_product_description'),
				'product_image' => $image,
				'stock_awal' => $this->input->post('f_stok'),
				'sisa_stock' => $this->input->post('f_stok'),
				'create_time' => current_time()
			);

			$this->product_m->insert_data($data);

			$price_qty = array(
				'uc_product' => $uc_product,
				'price_before_discount' => $this->input->post('f_price_before_discount'),
				'discount' => $this->input->post('f_discount'),
				'price_after_discount' => $this->input->post('f_price_after_discount'),
				'current_time' => current_time()
			);

			$this->product_price_m->insert_data($price_qty);
		}

		redirect('manage/product');
	}

	function edit(){
		$data = NULL;
		$js_uc = $this->input->post('js_uc');
		$data['row'] = $this->product_m->get_filtered(array('uc' => $js_uc))->row();

		$this->load->view('manage/product/edit',$data);
	}

	function update(){
		if ($this->input->post('f_save')) {

			$image  = $this->input->post('f_old_image');

			if(preg_match('/image/', @$_FILES['f_image']['type'])){
				if (isset($_POST['f_old_image'])) {
					if (@$_FILES['f_image']['type'] != NULL) {
						$image = $this->im_upload->replacing($this->input->post('f_old_image'), 'f_image', 'product');
					}
				}else{
					$image = $this->im_upload->uploading('f_image', 'product');
				}
			}


			$data = array(
				'uc_category' => $this->input->post('f_category'),
				'slug' => post_name($this->input->post('f_product_name')),
				'product_name' => $this->input->post('f_product_name'),
				'product_description' => $this->input->post('f_product_description'),
				'product_image' => $image
			);

			$where = array('uc' => $this->input->post('f_uc'));

			$this->product_m->update_data($data, $where);
		}

		redirect('manage/product');
	}

	function price($uc = NULL){
		if ($uc != NULL) {
			# code...
		}else{
			redirect('manage/product');
		}
	}


	function delete($uc = NULL){
		if ($uc != NULL) {
			$this->product_m->delete_data(array('uc' => $uc));
		}

		redirect('manage/product');
	}

	/*END PRODUCT*/


	/*CATEGORY PRODUCT*/

	function category(){
		$data = NULL;

		$page = 1;
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'product_cat_m'
						);

		$query = $this->product_cat_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->product_cat_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->im_render->main_admin('manage/category/list',$data);
	}

	function page_category(){
		$data = NULL;

		$page = ($this->input->post('js_page') != 1 ? $this->input->post('js_page') : 1);
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'product_cat_m'
						);

		$query = $this->product_cat_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->product_cat_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->load->view('manage/category/page',$data);
	}

	function add_cat(){
		$this->load->view('manage/category/add');
	}

	function insert_cat(){
		if ($this->input->post('f_save')) {
			$data = array(
				'uc' => unique_code(),
				'category' => $this->input->post('f_category'),
				'slug' => post_name($this->input->post('f_category'))
			);

			$this->product_cat_m->insert_data($data);
		}

		redirect('manage/product/category');
	}

	function edit_cat(){
		$data = NULL;

		$js_uc = $this->input->post('js_uc');

		$query = $this->product_cat_m->get_filtered(array('uc' => $js_uc));
		if ($query->num_rows() > 0) {
			$data['row'] = $query->row();
		}

		$this->load->view('manage/category/edit', $data);
	}

	function update_cat(){
		if ($this->input->post('f_save')) {
			$data = array(
				'category' => $this->input->post('f_category'),
				'slug' => post_name($this->input->post('f_category'))
			);

			$where = array('uc' => $this->input->post('f_uc'));

			$this->product_cat_m->update_data($data, $where);
		}

		redirect('manage/product/category');
	}

	function delete_cat($uc = NULL){
		if ($uc != NULL) {
			$this->product_cat_m->delete_data(array('uc' => $uc));
		}

		redirect('manage/product/category');
	}

	/*END CATEGORY PRODUCT*/
}