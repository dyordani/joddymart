<?php
Class Package extends CI_Controller{
	function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in())
	    {
	      	if (!$this->ion_auth->is_admin())
		    {
		      	redirect('auth');
		    }
	    }

	    $this->each_page 	= 20;
		$this->page_int 	= 5;

	    $this->load->model('package_m');

	}

	function index(){
		$data = NULL;

		$page = 1;
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'package_m'
						);

		$query = $this->package_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->package_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->im_render->main_admin('manage/package/list', $data);
	}

	function page(){
		$data = NULL;

		$page = ($this->input->post('js_page') != 1 ? $this->input->post('js_page') : 1);
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'package_m'
						);

		$query = $this->package_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->package_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->load->view('manage/package/page', $data);
	}

	function add(){
		$this->load->view('manage/package/add');
	}

	function insert(){
		if ($this->input->post('f_save')) {
			$data = array(
				'uc' => unique_code(),
				'package' => $this->input->post('f_package')
			);

			$this->package_m->insert_data($data);
		}

		redirect('manage/package');
	}

	function edit(){
		$data = NULL;

		$js_uc  = $this->input->post('js_uc');

		$data['row'] = $this->package_m->get_filtered(array('uc' => $js_uc))->row();

		$this->load->view('manage/package/edit', $data);
	}

	function update(){
		if ($this->input->post('f_save')) {
			$data = array(
				'package' => $this->input->post('f_package')
			);

			$where = array('uc' => $this->input->post('f_uc'));

			$this->package_m->update_data($data, $where);
		}

		redirect('manage/package');
	}

	function delete($uc = NULL){
		if ($uc != NULL) {
			$this->package_m->delete_data(array('uc' => $uc));
		}

		redirect('manage/package');
	}


}