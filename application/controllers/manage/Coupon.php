<?php
Class Coupon extends CI_Controller{
	function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in())
	    {
	      	if (!$this->ion_auth->is_admin())
		    {
		      	redirect('auth');
		    }
	    }

	    $this->each_page 	= 20;
		$this->page_int 	= 5;

	    $this->load->model('coupon_m');

	}

	function index(){
		$data = NULL;

		$page = 1;
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'coupon_m'
						);

		$query = $this->coupon_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->coupon_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->im_render->main_admin('manage/coupon/list', $data);
	}

	function page(){
		$data = NULL;

		$page = ($this->input->post('js_page') != 1 ? $this->input->post('js_page') : 1);
		//	Pagination Initialization
		$this->load->library('im_pagination');
		///	Define Offset
		$offset = ($page - 1) * $this->each_page;
		//	Define Parameters
		$params = array(
							'page_number'	=> $page,
							'each_page'		=> $this->each_page,
							'page_int'		=> $this->page_int,	
							'segment' 		=> 'category',						
							'model'			=> 'coupon_m'
						);

		$query = $this->coupon_m->get_all('id','DESC',$this->each_page,$offset);
		if ($query->num_rows() > 0) {
			$data['result'] = $query->result();
		}

		$query = $this->coupon_m->get_all('id','DESC');
		if ($query->num_rows() > 0) {
			$params['total_record']	= $query->num_rows();
			$data['pagination']	= $this->im_pagination->render_ajax($params);
		}

		$data['numbering'] 	= ($this->each_page * ($page-1)) + 1;

		$this->load->view('manage/coupon/page', $data);
	}

	function add(){
		$this->load->view('manage/coupon/add');
	}

	function insert(){
		if ($this->input->post('f_save')) {
			$data = array(
				'uc' => unique_code(),
				'code_coupon' => $this->input->post('f_code_coupon'),
				'value' => $this->input->post('f_value'),
				'start_date' => time_format($this->input->post('f_start_date'), 'Y-m-d'),
				'end_date' => time_format($this->input->post('f_end_date'), 'Y-m-d')
			);

			$this->coupon_m->insert_data($data);
		}

		redirect('manage/coupon');
	}

	function edit(){
		$data = NULL;

		$js_uc  = $this->input->post('js_uc');

		$data['row'] = $this->coupon_m->get_filtered(array('uc' => $js_uc))->row();

		$this->load->view('manage/coupon/edit', $data);
	}

	function update(){
		if ($this->input->post('f_save')) {
			$data = array(
				'code_coupon' => $this->input->post('f_code_coupon'),
				'value' => $this->input->post('f_value'),
				'start_date' => time_format($this->input->post('f_start_date'), 'Y-m-d'),
				'end_date' => time_format($this->input->post('f_end_date'), 'Y-m-d')
			);

			$where = array('uc' => $this->input->post('f_uc'));

			$this->coupon_m->update_data($data, $where);
		}

		redirect('manage/coupon');
	}

	function delete($uc = NULL){
		if ($uc != NULL) {
			$this->coupon_m->delete_data(array('uc' => $uc));
		}

		redirect('manage/coupon');
	}

}