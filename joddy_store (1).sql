-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2019 at 03:34 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `joddy_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'Operator', 'Untuk Operator');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '::1', 'admin@', 1558787936);

-- --------------------------------------------------------

--
-- Table structure for table `ol_blog`
--

DROP TABLE IF EXISTS `ol_blog`;
CREATE TABLE `ol_blog` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `image` varchar(225) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ol_blog`
--

INSERT INTO `ol_blog` (`id`, `uc`, `slug`, `title`, `description`, `author`, `image`, `create_time`, `is_exist`) VALUES
(1, 'OL-JD49005', 'post-title-here-kami-satu', 'Post Title Here & kami satu !', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.&lt;/p&gt;\r\n\r\n&lt;p&gt;Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.&lt;/p&gt;\r\n', 'Deri Ramadhan', 'IMG_2155.JPG', '2019-05-25 08:50:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ol_contact_form`
--

DROP TABLE IF EXISTS `ol_contact_form`;
CREATE TABLE `ol_contact_form` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ol_coupon`
--

DROP TABLE IF EXISTS `ol_coupon`;
CREATE TABLE `ol_coupon` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `code_coupon` varchar(30) NOT NULL,
  `value` double NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ol_coupon`
--

INSERT INTO `ol_coupon` (`id`, `uc`, `code_coupon`, `value`, `start_date`, `end_date`, `is_exist`) VALUES
(1, 'OL-JD35051', 'PROMO-RMD-2019', 150000, '2019-05-25', '2019-05-31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ol_customer`
--

DROP TABLE IF EXISTS `ol_customer`;
CREATE TABLE `ol_customer` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address_line_1` varchar(225) NOT NULL,
  `address_line_2` varchar(225) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zip_code` varchar(25) NOT NULL,
  `addtional_note` varchar(225) NOT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ol_invoice`
--

DROP TABLE IF EXISTS `ol_invoice`;
CREATE TABLE `ol_invoice` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `uc_customer` varchar(25) NOT NULL,
  `invoice_code` varchar(30) NOT NULL,
  `pay_method` tinyint(1) NOT NULL COMMENT '1 = Cash, 2 = Transfer',
  `pay_status` tinyint(1) NOT NULL COMMENT '1  = Un-paid, 2 = Paid',
  `cart_subtotal` double NOT NULL,
  `code_coupon` varchar(30) NOT NULL,
  `cart_total` double NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ol_orders`
--

DROP TABLE IF EXISTS `ol_orders`;
CREATE TABLE `ol_orders` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `uc_invoice` varchar(25) NOT NULL,
  `uc_product` varchar(25) NOT NULL,
  `qty` int(5) NOT NULL,
  `total` double NOT NULL,
  `note` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ol_package`
--

DROP TABLE IF EXISTS `ol_package`;
CREATE TABLE `ol_package` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `package` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ol_package`
--

INSERT INTO `ol_package` (`id`, `uc`, `package`) VALUES
(1, 'OL-JD34715', 'Gram'),
(2, 'OL-JD34722', 'Kg'),
(3, 'OL-JD34727', 'Pcs'),
(4, 'OL-JD34738', 'Lembar'),
(5, 'OL-JD34744', 'Karton');

-- --------------------------------------------------------

--
-- Table structure for table `ol_payment_confrim`
--

DROP TABLE IF EXISTS `ol_payment_confrim`;
CREATE TABLE `ol_payment_confrim` (
  `id` int(15) NOT NULL,
  `uc` int(25) NOT NULL,
  `uc_invoice` int(25) NOT NULL,
  `struk` varchar(225) NOT NULL,
  `payment_date` date DEFAULT NULL,
  `note` varchar(225) NOT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ol_product`
--

DROP TABLE IF EXISTS `ol_product`;
CREATE TABLE `ol_product` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `uc_category` varchar(25) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `uc_product` varchar(25) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `product_description` text NOT NULL,
  `product_image` varchar(225) DEFAULT NULL,
  `views` int(5) NOT NULL,
  `stock_awal` int(5) NOT NULL DEFAULT '0',
  `stock_keluar` int(5) NOT NULL DEFAULT '0',
  `sisa_stock` int(5) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `is_exist` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ol_product`
--

INSERT INTO `ol_product` (`id`, `uc`, `uc_category`, `slug`, `uc_product`, `product_name`, `product_description`, `product_image`, `views`, `stock_awal`, `stock_keluar`, `sisa_stock`, `create_time`, `is_exist`) VALUES
(1, 'OL-JD46082', 'OL-JD31139', 'organic-onion', '', 'Organic Onion', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolo</p>\r\n', 'big_product2.png', 0, 0, 0, 0, '2019-05-26 11:48:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ol_product_category`
--

DROP TABLE IF EXISTS `ol_product_category`;
CREATE TABLE `ol_product_category` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `category` varchar(100) NOT NULL,
  `is_exist` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ol_product_category`
--

INSERT INTO `ol_product_category` (`id`, `uc`, `slug`, `category`, `is_exist`) VALUES
(5, 'OL-JD31133', 'sembako', 'Sembako', 1),
(6, 'OL-JD31139', 'sayuran', 'Sayuran', 1),
(7, 'OL-JD31150', 'buah-buahan', 'Buah-buahan', 1),
(8, 'OL-JD31159', 'daging', 'Daging', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ol_product_price`
--

DROP TABLE IF EXISTS `ol_product_price`;
CREATE TABLE `ol_product_price` (
  `id` int(15) NOT NULL,
  `uc_product` varchar(25) NOT NULL,
  `price_before_discount` double NOT NULL,
  `discount` int(5) NOT NULL DEFAULT '0',
  `price_after_discount` double NOT NULL,
  `current_time` datetime DEFAULT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ol_product_price`
--

INSERT INTO `ol_product_price` (`id`, `uc_product`, `price_before_discount`, `discount`, `price_after_discount`, `current_time`, `is_exist`) VALUES
(1, 'OL-JD46082', 15000, 0, 0, '2019-05-26 11:48:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ol_product_review`
--

DROP TABLE IF EXISTS `ol_product_review`;
CREATE TABLE `ol_product_review` (
  `id` int(15) NOT NULL,
  `uc` varchar(25) NOT NULL,
  `uc_product` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` varchar(225) NOT NULL,
  `create_time` datetime NOT NULL,
  `is_exist` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$alRxXeOwtxfk2dvMAoKsRuOGulrMYbp42MsVA0PRZay4wg6BKEq3W', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1558869662, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'dyordhanideri@gmail.com', '$2y$10$ryx55CsWCiqAr7LclugknuMR9.I8c6Rre5/0Cg3a7A.3mc/RXAY8K', 'dyordhanideri@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1558772181, NULL, 1, 'Deri', 'Ramadhan', 'Joddy Store', '081384047644');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 2, 1),
(5, 2, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_blog`
--
ALTER TABLE `ol_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_contact_form`
--
ALTER TABLE `ol_contact_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_coupon`
--
ALTER TABLE `ol_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_customer`
--
ALTER TABLE `ol_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_invoice`
--
ALTER TABLE `ol_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_orders`
--
ALTER TABLE `ol_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_package`
--
ALTER TABLE `ol_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_payment_confrim`
--
ALTER TABLE `ol_payment_confrim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_product`
--
ALTER TABLE `ol_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_product_category`
--
ALTER TABLE `ol_product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_product_price`
--
ALTER TABLE `ol_product_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ol_product_review`
--
ALTER TABLE `ol_product_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ol_blog`
--
ALTER TABLE `ol_blog`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ol_contact_form`
--
ALTER TABLE `ol_contact_form`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ol_coupon`
--
ALTER TABLE `ol_coupon`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ol_customer`
--
ALTER TABLE `ol_customer`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ol_invoice`
--
ALTER TABLE `ol_invoice`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ol_orders`
--
ALTER TABLE `ol_orders`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ol_package`
--
ALTER TABLE `ol_package`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ol_payment_confrim`
--
ALTER TABLE `ol_payment_confrim`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ol_product`
--
ALTER TABLE `ol_product`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ol_product_category`
--
ALTER TABLE `ol_product_category`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ol_product_price`
--
ALTER TABLE `ol_product_price`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ol_product_review`
--
ALTER TABLE `ol_product_review`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
