$(document).ready(function(){
	$('.print').click(function(){
		$('.paper-landscape').css('box-shadow',' none');
		$('.paper-potrait').css('box-shadow',' none');
		$('.print').hide();
		window.print();
		$('.paper-landscape').css('box-shadow',' 0px 0px 3px #000');
		$('.paper-potrait').css('box-shadow',' 0px 0px 3px #000');
		$('.print').show();
	});
});