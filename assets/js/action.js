$(document).ready(function(){
		var base_url = $('#base-url').html();
		var name_token = $('input[name=f_token]').val();
		var value_token = $('input[name=f_value_token]').val();

		$('.btn-add').click(function(){
			$('.modal-content').load(base_url+'manage/product/add',{'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'});

		});

		$('.btn-edit').click(function(){
			var uc = $(this).attr('uc');
			var token

			$('.modal-content').load(base_url+'manage/product/edit',{js_uc : uc, '<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'});

		});

		$('.page a.pagination-ajax').click(function(){
			var page = $(this).attr('title');
			var product_name = $('input[name=f_product_fill]').val();
			var category = $('select[name=f_category_fill] option:selected').val();

			$('.load-data').load(
									base_url+'manage/product/page',{
										js_page : page,
										js_product_name : product_name,
										js_category : category,
										'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>'
									}
								);

			return false;
			
		});

		$('.btn-search').click(function(){
			var product_name = $('input[name=f_product_fill]').val();
			var category = $('select[name=f_category_fill] option:selected').val();
			var page = 1;

			$('.load-data').load(
									base_url+'manage/product/page',{
										js_page : page,
										js_product_name : product_name,
										js_category : category,
										csrf_joddy_store : value_token
									}
								);

			return false;
		});
	});